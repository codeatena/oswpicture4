//
//  SelectNameViewController.m
//  OSWPicture
//
//  Created by Admin on 6/23/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "SelectNameViewController.h"
#import "OSWPictureDefine.h"
#import "DBManager.h"
#import "OSWPictureName.h"
#import "AddNameAlertView.h"
#import "iToast.h"

@interface SelectNameViewController () <UITableViewDataSource, UITableViewDelegate, AddNameAlertViewDelegate> {
    NSMutableArray *arrNames;
    
    AddNameAlertView *alertView;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnBackground;

@end

@implementation SelectNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrNames = [NSMutableArray array];
    arrNames = [[DBManager sharedInstance] getAllNames];
    self.btnBackground.backgroundColor = [UIColor clearColor];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)show:(UIViewController *)parentView {
    [parentView.view addSubview:self.view];
    [parentView addChildViewController:self];
    
    // scale the button down before the animation...
    self.view.transform = CGAffineTransformMakeScale(0.75, 0.75);
    
    // now animate the view...
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         self.view.transform = CGAffineTransformIdentity;
                     }
                     completion:nil];
}

- (void)dismiss {
    // now animate the view...
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         self.view.transform = CGAffineTransformMakeScale(0.75, 0.75);
                     }
                     completion:^(BOOL finished){
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                     }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIButton delegate 
- (void)onAddNameClicked {
    alertView = [[AddNameAlertView alloc] init];
    alertView.delegate = self;
    [alertView showDialog];
}

- (IBAction)onBackgroundClicked:(id)sender {
    [self.delegate selectNameVCDismiss:self];
}

#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (arrNames.count+1);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)section {
    return (CGFloat)54.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    if (indexPath.row < arrNames.count ) {
        cell = [tableView dequeueReusableCellWithIdentifier:kNameTableViewCell];
        UILabel* label = (UILabel*) [cell viewWithTag:100];
        OSWPictureName *existName = (OSWPictureName *)[arrNames objectAtIndex:indexPath.row];
        [label setText:existName.name];
    } else if (indexPath.row == arrNames.count) {
        cell = [tableView dequeueReusableCellWithIdentifier:kAddNameTableViewCell];
        UIButton* button = (UIButton*) [cell viewWithTag:100];
        [button addTarget:self action:@selector(onAddNameClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < arrNames.count) {
        if (self.delegate != nil) {
            OSWPictureName *existName = (OSWPictureName *)[arrNames objectAtIndex:indexPath.row];
            [self.delegate didSelectedName:self name:existName.name];
        }
    }
    
    return;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if (indexPath.row == arrNames.count) {
        return NO;
    }
    
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        OSWPictureName *existName = (OSWPictureName *)[arrNames objectAtIndex:indexPath.row];
        [[DBManager sharedInstance] deleteName:existName.name];
        arrNames = [[DBManager sharedInstance] getAllNames];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

#pragma mark - AddNameAlertView delegate
- (void) clickedOkButton:(NSString *)newName {
    if ([[DBManager sharedInstance] addName:newName]) {
        if (self.delegate != nil) {
            [self.delegate didSelectedName:self name:newName];
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Name already exist!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void) clickedCancelButton {
    [alertView dismissWithClickedButtonIndex:-1 animated:YES];
}

@end
