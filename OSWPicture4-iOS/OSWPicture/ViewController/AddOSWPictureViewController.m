//
//  AddOSWPictureViewController.m
//  OSWPicture
//
//  Created by Admin on 6/22/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AddOSWPictureViewController.h"
#import "DBManager.h"
#import "OSWPictureName.h"
#import "OSWPictureImgList.h"
#import "OSWPictureDefine.h"
#import "AddNameAlertView.h"
#import "ShareManager.h"
#import "LxGridView.h"
#import "OSWFooterReusableView.h"
#import "OSWHeaderReusableView.h"

@interface AddOSWPictureViewController () <UITableViewDataSource, UITableViewDelegate, UIPrintInteractionControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate, LxGridViewCellDelegate> {
    NSMutableArray *arrImages;
    NSMutableArray *arrNames;
    
    NSMutableArray *_categories;
    NSMutableDictionary *_images;
    NSMutableArray *_switchValues;
    NSMutableArray *_sliderValues;

    NSMutableDictionary *_categoryDescriptions;
    
    AddNameAlertView *alertView;
    
    BOOL _isPrinting;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet LxGridView *collectView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnPrint;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottom;
@property (weak, nonatomic) IBOutlet UIImageView *imgTop;
@property (weak, nonatomic) IBOutlet UIImageView *imgMid;
@property (weak, nonatomic) IBOutlet UIView *viewPrinterBack;
@property (weak, nonatomic) IBOutlet UIView *viewRealPrinter;
@property (weak, nonatomic) IBOutlet UIWebView *printPreviewWebView;

@end

@implementation AddOSWPictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrImages = [NSMutableArray array];
    arrNames = [NSMutableArray array];
    arrNames = [[DBManager sharedInstance] getAllNames];
    
    _categoryDescriptions = [@
    {
        @"Dryrot"   : @"The common name for the wood-destroying fungus, Serpula lacrymans.",
        @"Rust"     : @"A reddish-orange fungus coating that forms on the surface of metal when exposed to air, water or dampness. This oxidized coating attacks and corrodes until it achieves total",
        @"Pest"     : @"Household pests that carry germs, bacteria, disease or viruses and have the capacity to cause structural damage in a home by gnawing, chewing or boring.",
        @"Mold"     : @"A living growth comprised of miniature fungi that forms on matter. It is often identified as a fuzzy or furry coating and is associated with decay or dampness.",
        @"Cracking" : @"To break or snap apart; to fall or break under pressure, fracture or failure of a wall, footer or floor.",
        @"Safety"   : @"The quality or condition of being safe. Freedom from danger, injury or damage, security.",
        @"Odors"    : @"The quality of something that affects the sense of smell. Musty, caused by dampness, decay or rot."
        
    } mutableCopy];
    
    _sliderValues = [NSMutableArray array];
    _switchValues = [NSMutableArray array];
    _categories = [NSMutableArray array];
    _images = [NSMutableDictionary dictionary];
    
    self.tableView.editing = YES;
    self.collectView.editing = YES;
    LxGridViewFlowLayout *layout = (LxGridViewFlowLayout *)self.collectView.collectionViewLayout;
    layout.headerReferenceSize = CGSizeMake(self.collectView.frame.size.width, 88);
    layout.footerReferenceSize = CGSizeMake(self.collectView.frame.size.width, 120);
    layout.minimumLineSpacing = 24;
    layout.sectionInset = UIEdgeInsetsMake(24, 24, 24, 24);
    
    [self.collectView registerNib:[UINib nibWithNibName:@"OSWHeaderReusableView" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    [self.collectView registerNib:[UINib nibWithNibName:@"OSWFooterReusableView" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
    
    if (self.name != nil && self.name.length > 0) {
        [[DBManager sharedInstance] addOSWPicture:self.name
                                            image:self.imgPhoto
                                            order:arrImages.count
                                         category:self.category
                                            label:self.label];
        arrImages = [[DBManager sharedInstance] getOSWPicturesWithName:self.name];
        
        [self setPicturesTitle:self.name];
    } else {
        if (arrNames.count > 0) {
            self.name = ((OSWPictureName *)[arrNames objectAtIndex:0]).name;
            arrImages = [[DBManager sharedInstance] getOSWPicturesWithName:self.name];
            
            [self setPicturesTitle:self.name];
        }
    }
    
    for (OSWPictureImgList *img in arrImages) {
        NSString *category = img.category;
        if (category == nil)
            category = @"Others";
        
        if (![_categories containsObject:category])
            [_categories addObject:category];
        
        NSMutableArray *array = _images[category];
        if (array == nil)
            array = [NSMutableArray array];
        
        _images[category] = array;
        
        [array addObject:img];
    }
    
    for (int i=0; i<[_categories count]; ++i) {
        [_sliderValues addObject:@(0)];
        [_switchValues addObject:@(YES)];
    }
    
    [_categories sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setPicturesTitle:(NSString *)name {
    NSString *szTitle = [NSString stringWithFormat:@"%@ PICTURES", name];
    self.lblTitle.text = szTitle;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark button click delegate
- (IBAction)onBackClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onShareClicked:(id)sender {
    if (self.name.length > 0) {
        [[ShareManager sharedInstance] shareZipFileUsingName:self name:self.name];
    }
}

- (void)onAddNameClicked {
    alertView = [[AddNameAlertView alloc] init];
    alertView.delegate = self;
    [alertView showDialog];
}

- (IBAction)onPrintClicked:(id)sender {
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    pic.delegate = self;
    
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputGeneral;
    printInfo.jobName = self.name;
    printInfo.duplex = UIPrintInfoDuplexLongEdge; // no duplex intended
    pic.printInfo = printInfo;
    pic.showsPageRange = YES;
    
    [self setNonPrintingElementsVisible:NO];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableArray *arrPages = [NSMutableArray array];
        UIImage* image = nil;
        NSMutableData *pdfData = [NSMutableData data];
        CGFloat scrollHeight = self.collectView.contentSize.height;
        
        CGSize pageSize = CGSizeMake(self.collectView.frame.size.width,self.collectView.frame.size.height);
        
        NSUInteger numberOfPages = ceilf(scrollHeight / pageSize.height);
        UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, pageSize.width, pageSize.height), nil);
        CGContextRef pdfContext = UIGraphicsGetCurrentContext();
        
        CGPoint savedContentOffset = self.collectView.contentOffset;
//        CGRect savedFrame = self.collectView.frame;
        
        BOOL usePhotoBitmap = NO; // use pdf instead
        
        self.collectView.contentOffset = CGPointZero;

        for (int i=0; i<numberOfPages; ++i) {
            if (usePhotoBitmap) {
                UIGraphicsBeginImageContext(pageSize);
            }
            UIGraphicsBeginPDFPage();
            
            CGFloat offsetForScroll = i * pageSize.height;
            self.collectView.contentOffset = CGPointMake(0, offsetForScroll);
            CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0, -offsetForScroll);
            
            [self.collectView.layer renderInContext:pdfContext];
            
            if (usePhotoBitmap) {
                [self.collectView.layer renderInContext:UIGraphicsGetCurrentContext()];
                image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                [arrPages addObject:image];
            }
        }
        UIGraphicsEndPDFContext();
        [self setNonPrintingElementsVisible:YES];
        
        self.viewPrinterBack.hidden = NO;

        [self.printPreviewWebView loadData:pdfData MIMEType:@"application/pdf" textEncodingName:nil baseURL:nil];
        
        self.collectView.contentOffset = savedContentOffset;
//        self.collectView.frame = savedFrame;
        
        if (usePhotoBitmap) {
            pic.printingItems = [arrPages copy];
        } else {
            pic.printingItem = pdfData;//[arrPages copy];
        }
        
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
            }
        };
        dispatch_async(dispatch_get_main_queue(),^{
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                [pic presentFromRect:self.btnPrint.bounds inView:self.btnPrint.viewForBaselineLayout animated:YES completionHandler:completionHandler];
                
            } else {
                [pic presentAnimated:YES completionHandler:completionHandler];
            }
        });
    });
}

- (void)setNonPrintingElementsVisible:(BOOL)isVisible
{
    self.collectView.editing = isVisible;
    _isPrinting = !isVisible;
    
    [self.collectView reloadData];
}

- (IBAction)onPrintCancelClicked:(id)sender {
    self.viewPrinterBack.hidden = YES;
}

#pragma mark - CollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [_categories count];
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_images[_categories[section]] count]; //arrImages.count;
}

- (UICollectionViewCell *)collectionView:(LxGridView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    LxGridViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.delegate = self;
    cell.editing = collectionView.editing;
    
    UIImageView *ivPicture = (UIImageView *)[cell viewWithTag:100];
    OSWPictureImgList *imgInfo = (OSWPictureImgList *)[arrImages objectAtIndex:indexPath.row];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:imgInfo.imgPath];
    [ivPicture setImage:[UIImage imageWithContentsOfFile:filePath]];

    return cell;
}

#pragma mark - LXGridView

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(330, 270);
}

- (void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)sourceIndexPath willMoveToIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSDictionary * dataDict = arrImages[sourceIndexPath.item];
    [arrImages removeObjectAtIndex:sourceIndexPath.item];
    [arrImages insertObject:dataDict atIndex:destinationIndexPath.item];
    [[DBManager sharedInstance] setOSWPicturesWithname:self.name images:arrImages];    
}

- (void)deleteButtonClickedInGridViewCell:(LxGridViewCell *)gridViewCell
{
    NSIndexPath * gridViewCellIndexPath = [self.collectView indexPathForCell:gridViewCell];
    
    if (gridViewCellIndexPath) {
        [[DBManager sharedInstance] deletePhoto:arrImages[gridViewCellIndexPath.row]];
        [arrImages removeObjectAtIndex:gridViewCellIndexPath.row];
        
        [self.collectView performBatchUpdates:^{
            [self.collectView deleteItemsAtIndexPaths:@[gridViewCellIndexPath]];
        } completion:nil];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView = nil;

    if (kind == UICollectionElementKindSectionHeader) {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        
        OSWHeaderReusableView *headerView = (OSWHeaderReusableView *)reusableView;
        
        headerView.category.text = _categories[indexPath.section];
    }
    
    if (kind == UICollectionElementKindSectionFooter) {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer" forIndexPath:indexPath];
        
        OSWFooterReusableView *footerView = (OSWFooterReusableView *)reusableView;
        
        footerView.descriptionLabel.text = _categoryDescriptions[_categories[indexPath.section]];
        
        footerView.slider.tag = indexPath.section;
        footerView.sliderSwitch.tag = indexPath.section;
        footerView.slider.value = [_sliderValues[indexPath.section] floatValue];
        footerView.sliderSwitch.on = [_switchValues[indexPath.section] boolValue];
        
        [footerView.slider removeTarget:self action:@selector(sliderDidChangeValue:) forControlEvents:UIControlEventValueChanged];
        [footerView.sliderSwitch removeTarget:self action:@selector(switchDidChangeValue:) forControlEvents:UIControlEventValueChanged];

        [footerView.slider addTarget:self action:@selector(sliderDidChangeValue:) forControlEvents:UIControlEventValueChanged];
        [footerView.sliderSwitch addTarget:self action:@selector(switchDidChangeValue:) forControlEvents:UIControlEventValueChanged];
        
        footerView.sliderSwitch.hidden = _isPrinting;
        footerView.slider.hidden = !footerView.sliderSwitch.isOn && _isPrinting;
    }
    
    return reusableView;
}

- (void)sliderDidChangeValue:(UISlider *)sender
{
    _sliderValues[sender.tag] = @(sender.value);
}

- (void)switchDidChangeValue:(UISwitch *)sender
{
    _switchValues[sender.tag] = @(sender.isOn);
}

#pragma mark - UITableView delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (arrNames.count+1);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)section {
    return (CGFloat)54.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    if (indexPath.row < arrNames.count ) {
        cell = [tableView dequeueReusableCellWithIdentifier:kNameTableViewCell];
        UILabel* label = (UILabel*) [cell viewWithTag:100];
        OSWPictureName *existName = (OSWPictureName *)[arrNames objectAtIndex:indexPath.row];
        [label setText:existName.name];
    } else if (indexPath.row == arrNames.count) {
        cell = [tableView dequeueReusableCellWithIdentifier:kAddNameTableViewCell];
        UIButton* button = (UIButton*) [cell viewWithTag:100];
        [button removeTarget:self action:@selector(onAddNameClicked) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(onAddNameClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < arrNames.count) {
        OSWPictureName *existName = (OSWPictureName *)[arrNames objectAtIndex:indexPath.row];
        arrImages = [[DBManager sharedInstance] getOSWPicturesWithName:existName.name];
        [self setPicturesTitle:existName.name];
        self.name = existName.name;
        [self.collectView reloadData];
    }
    
    return;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if (indexPath.row == arrNames.count) {
        return NO;
    }
    
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        OSWPictureName *existName = (OSWPictureName *)[arrNames objectAtIndex:indexPath.row];
        NSString *szName = existName.name;
        [[DBManager sharedInstance] deleteName:szName];
        arrNames = [[DBManager sharedInstance] getAllNames];
        
        //dispatch_async (dispatch_get_main_queue(), ^{
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        //});
        
        [[DBManager sharedInstance] deletePictures:szName];
        if (arrNames.count > 0) {
            NSString *szTitle = ((OSWPictureName *)[arrNames objectAtIndex:0]).name;
            arrImages = [[DBManager sharedInstance] getOSWPicturesWithName:szTitle];
            [self setPicturesTitle:szTitle];
            self.name = szTitle;
            [self.collectView reloadData];
        } else {
            arrImages = [NSMutableArray array];
            [self setPicturesTitle:@""];
            [self.collectView reloadData];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSString *nameToMove = arrNames[sourceIndexPath.row];
//    id imageToMove = arrImages[sourceIndexPath.row];
//    [arrImages removeObjectAtIndex:sourceIndexPath.row];
    [arrNames removeObjectAtIndex:sourceIndexPath.row];
    
//    [arrImages insertObject:imageToMove atIndex:destinationIndexPath.row];
    [arrNames insertObject:nameToMove atIndex:destinationIndexPath.row];
}


#pragma mark - AddNameAlertView delegate
- (void) clickedOkButton:(NSString *)newName {
    if ([[DBManager sharedInstance] addName:newName]) {
        arrNames = [[DBManager sharedInstance] getAllNames];
        [self.tableView reloadData];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Name already exist!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void) clickedCancelButton {
    [alertView dismissWithClickedButtonIndex:-1 animated:YES];
}

@end
