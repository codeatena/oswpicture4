//
//  ReportViewController.h
//  OSWPicture
//
//  Created by Code Atena on 12/8/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportViewController : UIViewController {
    IBOutlet UITextField*           tfName;
    IBOutlet UITextField*           tfAddress;
    IBOutlet UIButton*              btnDate;
    IBOutlet UIButton*              btnPrint;
    
    IBOutlet UITextView*            tvNotes;
    
    IBOutlet UITextField*           tfPrice;
    IBOutlet UITextField*           tfFT;
    IBOutlet UITextField*           tfOhioState;
    
    IBOutlet UIView*                viewContent;
}

@end
