//
//  MarkView.m
//  OSWPicture
//
//  Created by Admin on 6/18/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "MarkView.h"

@interface MarkView ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UITextField *tfContent;

@end

@implementation MarkView

- (id)init {
    self = [super init];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"MarkView" owner:self options:nil] lastObject];
        self.frame = CGRectMake(0, 0, kMarkViewWidth, kMarkViewHeight);
        self.lblContent.layer.cornerRadius = 5.0f;
        self.lblContent.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.1];
        self.tfContent.layer.cornerRadius = 5.0f;
        self.tfContent.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.1];
    }
    
    return self;
}

- (void)bindMark:(NSString *)title content:(NSString *)content {
    self.lblTitle.text = title;
    self.lblContent.text = content;
    self.tfContent.text = content;
    
    if ([content isEqualToString:@""]) {
        self.tfContent.hidden = NO;
        self.lblContent.hidden = YES;
        [self.tfContent becomeFirstResponder];
    } else {
        self.tfContent.hidden = YES;
        self.lblContent.hidden = NO;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
