//
//  OSWFooterReusableView.m
//  OSWPicture
//
//  Created by John Bennedict Lorenzo on 8/27/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "OSWFooterReusableView.h"

@implementation OSWFooterReusableView

- (void)awakeFromNib {
    // Initialization code
    
    [self.slider setThumbImage:[UIImage imageNamed:@"slider_knob"] forState:UIControlStateNormal];
    UIImage *sliderBG = [UIImage new];
    
    [self.slider setMinimumTrackImage:sliderBG forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:sliderBG forState:UIControlStateNormal];
}

@end
