//
//  OSWHeaderReusableView.h
//  OSWPicture
//
//  Created by John Bennedict Lorenzo on 8/27/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSWHeaderReusableView : UICollectionReusableView

@property (weak,nonatomic) IBOutlet UILabel *category;

@end
