//
//  OSWFooterReusableView.h
//  OSWPicture
//
//  Created by John Bennedict Lorenzo on 8/27/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSWFooterReusableView : UICollectionReusableView

@property (weak,nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak,nonatomic) IBOutlet UISwitch *sliderSwitch;
@property (weak,nonatomic) IBOutlet UISlider *slider;

@end
