//
//  DBManager.m
//  ProximityBLE
//
//  Created by Admin on 2/10/15.
//  Copyright (c) 2015 RuiFeng. All rights reserved.
//

#import "DBManager.h"
#import "AppDelegate.h"
#import "OSWPictureDefine.h"

static DBManager* _sharedInstance;

@implementation DBManager

- (AppDelegate *)appDelegate {
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

+ (DBManager*)sharedInstance {
    if (_sharedInstance == nil)
        _sharedInstance = [[DBManager alloc] init];
    
    return _sharedInstance;
}

- (NSMutableArray *)getAllNames {
    NSMutableArray *arrayEquipments = [[NSMutableArray alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:kOSWPictureNameEntity inManagedObjectContext:[self appDelegate].managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];

    NSError *error = nil;
    NSArray *fetchedObjects = [[self appDelegate].managedObjectContext executeFetchRequest:request error:&error];

    for (int i = 0; i < fetchedObjects.count; i++) {
        OSWPictureName *newName = (OSWPictureName *)[fetchedObjects objectAtIndex:i];
        [arrayEquipments addObject:newName];
    }

    return arrayEquipments;
}

- (BOOL)addName:(NSString *)newName {
    BOOL bRet = YES;
    if ([self isExistName:newName]) {
        bRet = NO;
    } else {
        OSWPictureName* addingName = [NSEntityDescription insertNewObjectForEntityForName:kOSWPictureNameEntity inManagedObjectContext:[self appDelegate].managedObjectContext];
            addingName.name = newName;
            NSError* error = nil;
            if (![[self appDelegate].managedObjectContext save:&error]) {
                bRet = NO;
            } else {
                bRet = YES;
            }
    }
    
    return bRet;
}

- (BOOL)deleteName:(NSString *)oldName {
    BOOL bRet = YES;
    if ([self isExistName:oldName] == NO) {
        bRet = NO;
    } else {
        OSWPictureName *existName = [self getExistName:oldName];
        if (existName == nil) {
            bRet = YES;
        } else {
            NSError* error = nil;
            [[self appDelegate].managedObjectContext deleteObject:existName];
            if (![[self appDelegate].managedObjectContext save:&error]) {
                bRet = NO;
            } else {
                bRet = YES;
            }
        }
    }
    
    return bRet;
}

- (BOOL)isExistName:(NSString *)name {
    NSMutableArray *arrayNames = [[NSMutableArray alloc] init];
    arrayNames = [self getAllNames];

    for (int i = 0; i < arrayNames.count; i++) {
        OSWPictureName *savedName = (OSWPictureName *) [arrayNames objectAtIndex:i];
        if ([savedName.name isEqualToString:name]) {
            return YES;
        }
    }

    return NO;
}
- (OSWPictureName *)getExistName:(NSString *)name {
    OSWPictureName *existName = nil;
    
    NSMutableArray *arrayNames = [[NSMutableArray alloc] init];
    arrayNames = [self getAllNames];
    
    for (int i = 0; i < arrayNames.count; i++) {
        OSWPictureName *savedName = (OSWPictureName *) [arrayNames objectAtIndex:i];
        if ([savedName.name isEqualToString:name]) {
            existName = savedName;
        }
    }
    
    return existName;
}

- (BOOL)addOSWPicture:(NSString *)name
                image:(UIImage *)image
                order:(NSUInteger)order
             category:(NSString *)category
                label:(NSString *)label
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    [dateformatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateInStringFormatted = [dateformatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.png", dateInStringFormatted];
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
    
    // Save image.
    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
    
    OSWPictureImgList* addingImage = [NSEntityDescription insertNewObjectForEntityForName:kOSWPictureImgListEntity inManagedObjectContext:[self appDelegate].managedObjectContext];
    addingImage.name = name;
    addingImage.imgPath = fileName;
    addingImage.category = category;
    addingImage.label = label;
    addingImage.order = [NSNumber numberWithInteger:order];
    
    NSError* error = nil;
    if (![[self appDelegate].managedObjectContext save:&error]) {
        return NO;
    }
    
    return YES;
}

- (NSMutableArray *)getOSWPicturesWithName:(NSString *)name {
    NSMutableArray *arrList = [NSMutableArray array];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kOSWPictureImgListEntity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name==%@", name]; // If required to fetch specific vehicle
    fetchRequest.predicate = predicate;
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]];
    
    NSArray *fetchedObjects = [[self appDelegate].managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    for (int i = 0; i < fetchedObjects.count; i++) {
        OSWPictureImgList *newImage = (OSWPictureImgList *)[fetchedObjects objectAtIndex:i];
        [arrList addObject:newImage];
    }
    
    return arrList;
}

// For moving images
- (void)setOSWPicturesWithname:(NSString *)name images:(NSArray *)images {
    int i = 0;
    for (OSWPictureImgList *image in images) {
        image.order = [NSNumber numberWithInteger:i];
        i++;
    }
    
    NSError *error = nil;
    if (![[self appDelegate].managedObjectContext save:&error]) {
        return;
    }
}

- (void)deletePhoto:(OSWPictureImgList *)image
{
    [[self appDelegate].managedObjectContext deleteObject:image];
}

- (BOOL)deletePictures:(NSString *)oldName {
    BOOL bRet = YES;
    NSMutableArray *arrDatas = [self getOSWPicturesWithName:oldName];
    for (OSWPictureName * item in arrDatas) {
        [[self appDelegate].managedObjectContext deleteObject:item];
    }
    
    return bRet;
}

@end
