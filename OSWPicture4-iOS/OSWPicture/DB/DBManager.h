//
//  DBManager.h
//  ProximityBLE
//
//  Created by Admin on 2/10/15.
//  Copyright (c) 2015 RuiFeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OSWPictureName.h"
#import "OSWPictureImgList.h"

@interface DBManager : NSObject {
}

+ (DBManager*)sharedInstance;

- (NSMutableArray *)getAllNames;
- (BOOL)addName:(NSString *)newName;
- (BOOL)deleteName:(NSString *)oldName;
- (BOOL)isExistName:(NSString *)name;
- (OSWPictureName *)getExistName:(NSString *)name;
- (BOOL)addOSWPicture:(NSString *)name
                image:(UIImage *)image
                order:(NSUInteger)order
             category:(NSString *)category
                label:(NSString *)label;

- (NSMutableArray *)getOSWPicturesWithName:(NSString *)name;
- (BOOL)deletePictures:(NSString *)oldName;
- (void)deletePhoto:(OSWPictureImgList *)image;

- (void)setOSWPicturesWithname:(NSString *)name images:(NSArray *)images;

@end
