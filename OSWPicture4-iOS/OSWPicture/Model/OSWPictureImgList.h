//
//  OSWPictureImgList.h
//  OSWPicture
//
//  Created by Admin on 6/23/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OSWPictureImgList : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * imgPath;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * label;

@end
