//
//  ShareManager.h
//  Project
//
//  Created by Mountain on 6/24/13.
//  Copyright (c) 2013 Qingxin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@class ZipArchive;

@interface ShareManager : NSObject <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) ZipArchive*   currentArchive;

+ (ShareManager*)sharedInstance;

- (void)shareZipFileUsingName:(UIViewController *)vc name:(NSString *)name;

@end
