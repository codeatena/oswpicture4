//
//  ShareManager.m
//  Project
//
//  Created by Mountain on 6/24/13.
//  Copyright (c) 2013 Qingxin. All rights reserved.
//

#import "ShareManager.h"
#import "ZipArchive.h"
#import "AppDelegate.h"
#import "DBManager.h"
#import "OSWPictureImgList.h"

@implementation ShareManager

static ShareManager* _sharedInstance;

@synthesize currentArchive;

+ (ShareManager*)sharedInstance {
    if (_sharedInstance == nil)
        _sharedInstance = [[ShareManager alloc] init];
    
    return _sharedInstance;
}

- (void)shareZipFileUsingName:(UIViewController *)vc name:(NSString *)name {
    NSMutableArray *arrImages = [[DBManager sharedInstance] getOSWPicturesWithName:name];
    
    NSString* dPath = NSTemporaryDirectory();
    NSString* zipfile = [dPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@pictures.zip", name]];
    ZipArchive* zip = [[ZipArchive alloc] init];
    BOOL ret = [zip CreateZipFile2:zipfile];
    if (ret) {
        self.currentArchive = zip;
        [self appendFilesToZip:arrImages];
        [self shareViaEmail:vc name:name];
    }
}

- (void)appendFilesToZip:(NSMutableArray *)arrImages {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : @"";
    for (OSWPictureImgList* savedImage in arrImages) {
        NSString *imgPath = [basePath stringByAppendingPathComponent: savedImage.imgPath];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:imgPath]) {
            NSData *data = [[NSFileManager defaultManager] contentsAtPath:imgPath];
            [self writeDataToFile: data file: savedImage.imgPath];
            [self addFileToZip: savedImage.imgPath];
        }
        else {
            NSLog(@"File not exists");
        }
    }
    
    [self.currentArchive CloseZipFile2];
}

- (void)addFileToZip:(NSString*) filename {
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent: filename];    
    [self.currentArchive addFileToZip: filePath newname: filename];
}

- (void)writeDataToFile:(NSData*)data file:(NSString*)filename {
    NSURL *fURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent: filename]];
    [data writeToURL: fURL atomically: YES];
}

- (void)shareViaEmail:(UIViewController *)vc name:(NSString *)name {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.navigationBar.tintColor = [UIColor darkGrayColor];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:@"Picture Files"];
        
        NSURL *zipfile = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@pictures.zip", name]]];
        NSData* data = [NSData dataWithContentsOfURL: zipfile];
        [mailViewController addAttachmentData: data mimeType:@"application/zip" fileName:[NSString stringWithFormat:@"%@pictures.zip", name]];
        
        [vc presentViewController: mailViewController animated: YES completion: nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"iPad could not send an email!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark MailComposeViewController Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error; {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Cancelled sending");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Message Saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Message Sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Sending Failed");
            break;
        default:
            NSLog(@"Message not sent");
            break;
    }
    UIViewController* rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootViewController dismissViewControllerAnimated: YES completion: nil];
}

@end
